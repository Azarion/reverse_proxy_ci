#!/bin/bash

if [ "x" == "x$1" ]; then
	echo "use $0 <proxyurl>"
	exit 1
else
	PROXYURL=$1
fi

echo Proxying to $PROXYURL

apt update -y
apt install nginx -y
unlink /etc/nginx/sites-enabled/default
echo "server {
        listen 80;
        location / {
        proxy_pass $PROXYURL;
        }
    }" > /etc/nginx/sites-available/reverse-proxy.conf
ln -s /etc/nginx/sites-available/reverse-proxy.conf /etc/nginx/sites-enabled/reverse-proxy.conf
service nginx configtest
service nginx restart
